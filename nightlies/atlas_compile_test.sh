#!/bin/sh
echo START TEST atlas_compile_test  `date`
echo "TestArea=${TestArea}"
compile_area="${TestArea}/tmp_cmplr"
if [ -d $compile_area ]; then rm -rf $compile_area; fi
mkdir -p ${compile_area}/LArCalorimeter 
touch ${TestArea}/PoolFileCatalog.xml
echo "<stupidtag>nothing</stupidtag>" >> ${TestArea}/PoolFileCatalog.xml 
#cp -a /cvmfs/atlas-nightlies.cern.ch/repo/sw/tmp/LArRecEvent ${compile_area}/LArCalorimeter
cp -a /cvmfs/atlas-nightlies.cern.ch/repo/sw/tests_nightlies/x86*/20.1.X/rel_*/LArCalorimeter/LArRecEvent ${compile_area}/LArCalorimeter
if [ -d ${compile_area}/LArCalorimeter/LArRecEvent ]; then rm -rf ${compile_area}/LArCalorimeter/LArRecEvent/x86*; fi
cd ${compile_area}/LArCalorimeter/LArRecEvent/cmt
export CMTPATH=${compile_area}:$CMTPATH
echo "CMTPATH=${CMTPATH}"
echo "list current dir:"
ls -ltr
echo "list compile area ${compile_area}"
ls -ltr ${compile_area}
echo "list test area ${TestArea}"
ls -ltr ${TestArea}
cmt config
. ./setup.sh
make
stat=$?
echo END TEST atlas_compile_test  `date`
exit $stat