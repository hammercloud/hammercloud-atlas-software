#! /bin/env python2

import sys
try:
    import simplejson as json
except:
    import json

def cleanJson(input):
    output = {}
    for line in input.splitlines():
        if line.startswith('['):
            token=line.replace('[','').replace(']','')
            try:
                output[token] = ['{']
            except: pass
            continue
        try:
            if not line.startswith('#') and not token in line:
                output[token].append(line)
        except(UnboundLocalError):
            pass

    outl = {}
    for k in output:
        outl[k] = '\n'.join(output[k])
    return outl
def validate(iFile):
    if (iFile[-1].rstrip()):
        print('Missing empty line at end of tpl-file!')
        sys.exit(1)
    try:
        data = json.loads(iFile)
    except Exception as e:
        print('Problem with json file')
        print(e)
        sys.exit(1)
    else:
        print('All fine!')
        sys.exit(0)

def printUsage():
    print('Usage: {} [tpl file to validate]'.format(sys.argv[0]))
    sys.exit(1)

if __name__ == '__main__':
    
    try:
        ifname = sys.argv[1]
    except:
        printUsage()
    try:
        iFile = open(ifname,'r')
    except(IOError):
        print('Unable to open file \"{}\"'.format(ifname))
        sys.exit(0)
    print('Validating template file \"{}\"'.format(ifname))
    cleaned = cleanJson(iFile.read())
    print('Found {} json object(s) in file: {}'.format(len(cleaned),', '.join(cleaned)))
    for k in cleaned:
        validate(cleaned[k])

