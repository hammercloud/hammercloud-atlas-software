"""
# ppjson.py ... Print pretty json
#
#
#
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
# Authors:
# - Valentina Mancinelli, <valentina.mancinelli@cern.ch>, 2015-2016
# - Jaroslava Schovancova, <jaroslava.schovancova@cern.ch>, 2016
#
"""
import json
import os
import sys
import traceback


if __name__ == '__main__':
    try:
        argv = sys.argv[1:]
        if len(argv)==1:
            print('INFO\tRunning:\t%s %s' %(sys.argv[0], ' '.join(argv) ))
            fname_in = argv[0]
            fname_out = fname_in+'.pretty.json'

        if len(argv)<1:
            print('ERROR\tUsage:\t%s /path/to/file.json' % (sys.argv[0]))
            exit(1)
        else:
            print('INFO\tUsage:\t%s /path/to/file.json' % (sys.argv[0]))

        f_in = open(fname_in)
        data = json.loads(f_in.read())
        f_in.close()


        if os.path.exists(fname_out):
            print('CRITICAL\tOutput file already exists: %s' % (fname_out))
        else:
            f_out = open(fname_out, 'w')
            f_out.write(json.dumps(data, sort_keys=True, indent=2))
            f_out.close()

            print('INFO\tRead %s' % (fname_in))
            print('INFO\tWrote %s' % (fname_out))

    except Exception:
        traceback.print_exc()
    else:
        print('INFO\tAll succeded - Over and out')


