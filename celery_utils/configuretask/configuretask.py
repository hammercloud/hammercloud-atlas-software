#! /bin/env python3
"""
# configuretask.py ... Prepare a configuration file for a JEDI task for HammerCloud,
#                      based on a JEDI task ID of existing task. HC-552
#
#
#
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
# Authors:
# - Valentina Mancinelli, <valentina.mancinelli@cern.ch>, 2015-2016
# - Jaroslava Schovancova, <jaroslava.schovancova@cern.ch>, 2016
# - David Hohn, <david.hohn@cern.ch>, 2018-2021
#
"""
import argparse
import collections
import json
import os
import pprint
import subprocess
import sys
import tempfile
import textwrap
import traceback
from datetime import datetime


def setupPandaClient():
    global Client
    import sys

    import pandatools
    sys.modules["taskbuffer"] = pandatools
    import taskbuffer
    from taskbuffer import Client as hiddenClient
    Client = hiddenClient


def dict_replace_value(d, old, new):
    x = {}
    for k, v in d.items():
        if isinstance(v, dict):
            v = dict_replace_value(v, old, new)
        elif isinstance(v, list):
            v = list_replace_value(v, old, new)
        elif isinstance(v, str):
            v = v.replace(old, new)
        x[k] = v
    return x


def list_replace_value(l, old, new):
    x = []
    for e in l:
        if isinstance(e, list):
            e = list_replace_value(e, old, new)
        elif isinstance(e, dict):
            e = dict_replace_value(e, old, new)
        elif isinstance(e, str):
            e = e.replace(old, new)
        x.append(e)
    return x


def deep_update(source, overrides):
    """
    Update a nested dictionary or similar mapping.
    Modify ``source`` in place.
    """
    for key, value in overrides.items():
        if isinstance(value, collections.abc.Mapping) and value:
            if key not in source:
                continue
            returned = deep_update(source.get(key, {}), value)
            source[key] = returned
        else:
            source[key] = overrides[key]
    return source


class ConfigureTask:
    """
    ConfigureTask ... class to prepare a configuration file for a JEDI task for HammerCloud,
                     based on a JEDI task ID of existing task. HC-552

    """
    def __init__(self, jeditaskid=None, from_PandaClient=False, output_dir=None, file_name=None):
        """
        Constructor
        """
        ### jeditaskid
        if jeditaskid is not None:
            self.jeditaskid = jeditaskid
        else:
            print('ERROR\tUnknown JEDI task ID " %s "! Exiting.' % (jeditaskid))
            raise RuntimeError()
        ### from_PandaClient
        ### when true get the taskparams from the PandaClient
        ### when false get the taskparams from bigpanda
        self.from_PandaClient = from_PandaClient
        ### input/output file names
        self.date = datetime.utcnow().isoformat()
        if output_dir:
            self.tpldir = output_dir
        else:
            self.tpldir = tempfile.gettempdir()
        if file_name:
            self.file_name = file_name
        else:
            self.file_name = self.get_output_file_name()
        self.file_name_input = self.file_name+'_INPUT'
        ### result
        self.result = {}
        self.taskparams = {}

    def run(self):
        """
        run ... run this to get the template configuration file draft.
        """
        print('INFO\tLet\'s begin.')

        if self.from_PandaClient:
            self.load_taskparams_from_PandaClient()
        else:
            url = self.get_task_url(jeditaskid=self.jeditaskid)
            print('INFO\tTask page URL: %s' % (url))
            _ = self.fetch_task_info(url, self.file_name_input)
            self.load_json()
        print('INFO\tCrunching data.')

        self.process_task_parameters()

        print('INFO\tGot result.')
        res = self.format_result()
        self.write_file(res, self.file_name)
        print('INFO\tWrote result in file %s' % (self.file_name))
        print('INFO\tThat\'s all, folks!')

    def get_jobParameters_template_name(self):
        '''
        were looking for a string like "user.dhohn.$JEDITASKID._${SN/P}.benchmark-result-github-DB12-core-4-no-ca.txt"
        and trying to replace the user/campaign specific part + jedi task id with the ###OUTPUTDATASETNAME### eventually.
        The ${SN/P} seems to be important for reactivating the task
        '''
        if "jobParameters" in self.taskparams:
            for jp in self.taskparams["jobParameters"]:
                if "param_type" in jp and jp['param_type'] == 'output':
                    return '.'.join(jp['value'].split('.')[:3])
        return None

    def process_task_parameters(self):
        print('FIXME\tprocess_task_parameters()')
        if not self.taskparams:
            self.taskparams = self.result['taskparams']

        # turn {'name':'thisname','value':'thisvalue'} into {'thisname':'thisvalue'}
        for i, tp in enumerate(self.taskparams):
            if len(tp)==2 and "name" in tp and "value" in tp:
                self.taskparams[i] = {tp['name']:tp['value']}

        jobParameters_template_name = self.get_jobParameters_template_name()
        print(f"replaced str: {jobParameters_template_name}")
        if jobParameters_template_name:
            self.taskparams = dict_replace_value(self.taskparams, jobParameters_template_name, "###OUTPUTDATASETNAME###")

        orig_taskName = self.taskparams['taskName']
        orig_taskName = orig_taskName.strip('/')
        self.taskparams = dict_replace_value(self.taskparams, orig_taskName, "###OUTPUTDATASETNAME###")

        try:
            orig_sourceURL = self.taskparams['sourceURL']
            self.taskparams = dict_replace_value(self.taskparams, orig_sourceURL, "###SOURCESURL###")
        except KeyError:
            pass


        default_keywords = {"site"          :"###SITE###",
                            "sourceURL"     :"###SOURCESURL###",
                            "taskName"      :"###TASKNAME###",
                            "processingType":"gangarobot",
                            "userName"      :"gangarbt",
                            "buildSpec"     : {"archiveName"   :"###SOURCESTARBALL###"},
                            }
        delete_keywords = ['countryGroup']
        for key in delete_keywords:
            if key in self.taskparams.keys():
                del self.taskparams[key]
        deep_update(self.taskparams, default_keywords)
        # for key in self.taskparams:
        #     if key in default_keywords:
        #         self.taskparams[key] = default_keywords[key]

        for key in self.taskparams:
            if "jobParameters" == key:
                for jp in self.taskparams["jobParameters"]:
                    try:
                        if jp["param_type"] == 'log':
                            #this looks wrong FIX
                            # is this for ES jobs?
                            del tp["jobParameters"][i]
                    except KeyError:
                        pass

        pprint.pprint(self.taskparams)

    def get_output_file_name(self):
        """
        get_output_file_name ... get file_name for output, the template configuration file draft.
        """
        if not os.path.exists(self.tpldir):
            print('ERROR Template dir %s does not exist! Exiting.' % (self.tpldir))
            sys.exit()
        fn = 'draft_template_from_%s.tpl.%s' % (self.jeditaskid, self.date)
        fname = os.path.join(self.tpldir, fn)
        return fname

    def get_task_url(self, jeditaskid):
        """
        get_task_url ... get URL of a task page in json format in BigPanDA monitor
        """
        return f'http://bigpanda.cern.ch/task/{jeditaskid}/?json'

    def fetch_task_info(self, url, file_name):
        """
        fetch_task_info ... download URL content into a file.
        """

        cmd = "curl '%(url)s' -o %(file_name)s " % {'url': url, 'file_name': file_name}
        cp = subprocess.run(cmd, shell=True)
        st = cp.returncode
        tmpo = cp.stdout
        if st == 0:
            print('INFO\tFetched content of URL %s into file %s' % (url, file_name))
        else:
            print('CRITICAL\tFailed to fetch content of URL %s into file %s. Error code: %s Issue: %s' % (url, file_name, st, repr(tmpo)))
            sys.exit('%s:%s' % (st, repr(tmpo)))
        return st

    def load_json(self):
        with open(self.file_name_input, 'r') as f:
            self.result = json.loads(f.read())

    def load_taskparams_from_PandaClient(self):
        s, o = Client.getTaskParamsMap(self.jeditaskid)
        if s != 0:
            print(f"ERROR\tCouldn't get taskparams from PandaClient for jeditaskid={self.jeditaskid}")
            raise RuntimeError()
        else:
            self.taskparams = json.loads(o)
            self.write_file(self.pretty_print(self.taskparams), self.file_name_input)

    def pretty_print(self, data):
        return textwrap.indent(json.dumps(data, sort_keys=True, indent=2), "  ")

    def write_file(self, data, file_name):
        with open(file_name, 'w') as f:
            f.write(data)

    def format_result(self):
        res = ''
        # header
        res += f'''\
### clone of JEDI task {self.get_task_url(jeditaskid=self.jeditaskid)}
### HC template draft, created on {self.date}
###
'''

        res += f'''
######################
##### taskparams #####
######################
[taskparams]
taskparams = {self.pretty_print(self.taskparams)}

'''
        return res


if __name__ == '__main__':
    try:
        p = argparse.ArgumentParser()
        p.add_argument("--jeditaskid", type=str, required=True)
        p.add_argument("--usePandaClient", action="store_true", default=True)
        # PandaClient gives more reliable taskParams and is new default
        p.add_argument("--out", "-o", type=str, default=".")
        p.add_argument("--file-name", type=str)
        global args
        args = p.parse_args()

        if args.usePandaClient:
            print("Using PandaClient.")
            setupPandaClient()

        ct = ConfigureTask(jeditaskid=args.jeditaskid,
                           from_PandaClient=args.usePandaClient,
                           output_dir=args.out,
                           file_name=args.file_name)
        ct.run()

    except RuntimeError:
        traceback.print_exc()
    else:
        print('INFO\tAll succeded - Over and out')
