"""
# configurejob.py ... Prepare a configuration file for a PanDA job for HammerCloud,
#                     based on a PanDA job ID of existing job. HC-408
#
#
#
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
# Authors:
# - Valentina Mancinelli, <valentina.mancinelli@cern.ch>, 2015-2016
# - Jaroslava Schovancova, <jaroslava.schovancova@cern.ch>, 2016
#
"""
import commands
import json
import os
import re
import sys
import traceback
from datetime import datetime



class ConfigureJob:
    """
    ConfigureJob ... class to prepare a configuration file for a PanDA job for HammerCloud,
                     based on a PanDA job ID of existing job. HC-408

    """
    def __init__(self, pandaid=None, jobparametersfile='/tmp/jobparameters_file.txt', \
                 buildpandaid=None, buildjobparametersfile='/tmp/jobparameters_file.txt'):
        """
        Constructor
        """
        ### pandaid
        if pandaid is not None:
            self.pandaid = pandaid
        else:
            print('ERROR\tUnknown PanDA ID " %s "! Exiting.' % (pandaid))
        if buildpandaid is None:
            print('WARNING\tUnknown PanDA ID of a build job " %s ".' % (buildpandaid))
        self.buildpandaid = buildpandaid
        ### input/output file names
        self.hash = datetime.utcnow().strftime('%F.%H%M%S')
        self.tpldir = '/tmp/'
        self.filename = self.get_output_filename()
        self.filename_input = self.filename+'_INPUT'
        self.filename_input_build = self.filename+'_BUILD'+'_INPUT'
        self.jobparametersfile = jobparametersfile
        self.buildjobparametersfile = buildjobparametersfile
        ### JobSpec field mapping
        self.jobspec_defaults = self.get_jobspec_defaults()
        self.jobspec_fields = self.get_jobspec_fields()
        self.jobspec_fields_selected = [\
#             'computingSite', 'cloud', 'jobName', 'jobDefinitionID', 'prodDBlock', 'destinationDBlock',
            'AtlasRelease', 'homepackage', 'transformation', 'VO',
            'prodSourceLabel','processingType','currentPriority', 'cmtConfig',
            'prodUserName', 'inputFileType', 'jobParameters'
        ]
        ### FileSpec field mapping
        self.filespec_fields = self.get_filespec_fields()
        self.filespec_fields_selected_in = ['type', 'lfn', 'dataset', 'prodDBlock', 'GUID', 'fsize', 'scope', 'status', 'md5sum', 'checksum']
        self.filespec_fields_selected_out = ['type', 'lfn', 'dataset', 'destinationDBlock']
        ### result
        self.result={}
        self.result['job']={}
        self.result['job_extra']={}
        self.result['files']={}
        self.result['files_extra']={}
        self.result['input_data_exploration_extra']={}
        self.result['buildjob']={}
        self.result['buildjob_extra']={}
        self.result['buildfiles']={}
        self.result['buildfiles_extra']={}


    def run(self):
        """
        run ... run this to get the template configuration file draft.
        """
        print('INFO\tLet\'s begin.')
        ### work job
        url = self.get_job_url(pandaid=self.pandaid)
        print('INFO\tJob page URL: %s' % (url))
        st = self.fetch_job_info(url, self.filename_input)

        if self.buildpandaid is not None:
            ### build job
            buildurl = self.get_job_url(pandaid=self.buildpandaid)
            print('INFO\tJob page URL: %s' % (buildurl))
            st = self.fetch_job_info(buildurl, self.filename_input_build)

        self.load_data()
        self.jobparameters = self.read_jobparameters(self.jobparametersfile)
        if self.buildpandaid is not None:
            self.jobparametersbuild = self.read_jobparameters(self.buildjobparametersfile)
        else:
            self.jobparametersbuild = ''
        print('INFO\tCrunching data.')
        ### workjob
        self.process_files(jobtype='work')
        self.process_job(jobtype='work')
        self.process_jobparameters(jobtype='work')
        ### buildjob
        if self.buildpandaid is not None:
            self.process_files(jobtype='build')
            self.process_job(jobtype='build')
            self.process_jobparameters(jobtype='build')
        print('INFO\tGot result.')
        res = self.format_result()
        self.write_file(res, self.filename)
        print('INFO\tWrote result in file %s' % (self.filename))
        print
        print('INFO\tThat\'s all, folks!')


    def process_jobparameters(self, jobtype='work'):
        if jobtype == 'work':
            jp = self.jobparameters
            files_key = 'files'
            job_key = 'job'
            jobextra_key = 'job_extra'
        elif jobtype == 'build':
            jp = self.jobparametersbuild
            files_key = 'buildfiles'
            job_key = 'buildjob'
            jobextra_key = 'buildjob_extra'
        ### match files
        file_identifiers = sorted(list(set([k.split('.')[0] for k in self.result[files_key].keys()])))
        ### avoid pseudo_inputs
        relevant_types = ['input', 'output', 'log']
        for fk in file_identifiers:
            file_id = fk
            k = '%s.type' % (file_id)
            file_type = self.result[files_key][k]
            ### avoid pseudo_inputs
            if file_type in relevant_types:
                k = '%s.lfn' % (file_id)
                file_lfn = self.result[files_key][k]
                new_id = '###%s###' % (file_id.upper())
                jp = re.sub(file_lfn, new_id, jp)
                self.result[jobextra_key][new_id] = '###%s###' % (file_id)

        self.result[job_key]['jobParameters'] = jp


    def get_output_filename(self):
        """
        get_output_filename ... get filename for output, the template configuration file draft.
        """
        if not os.path.exists(self.tpldir):
            print('ERROR Template dir %s does not exist! Exiting.' % (self.tpldir))
            sys.exit()
        fn = 'draft_template_from_%s.tpl.%s' % (self.pandaid, self.hash)
        fname = os.path.join(self.tpldir, fn)
        return fname


    def get_job_url(self, pandaid):
        """
        get_job_url ... get URL of a job page in json format in BigPanDA monitor
        """
        url = 'http://bigpanda.cern.ch/job?json&pandaid=%s' % (pandaid)
        return url


    def fetch_job_info(self, url, filename):
        """
        fetch_job_info ... download URL content into a file.
        """
        cmd = "curl '%(url)s' -o %(filename)s " % {'url':url, 'filename':filename}
        st=0
        st, tmpo = commands.getstatusoutput(cmd)
        if st==0:
            print('INFO\tFetched content of URL %s into file %s' % (url, filename))
        else:
            print('CRITICAL\tFailed to fetch content of URL %s into file %s. Error code: %s Issue: %s' % (url, filename, st, repr(tmpo)))
            sys.exit('%s:%s' % (st, repr(tmpo)))
        return st


    def get_jobspec_defaults(self):
        js_defaults={}
        js_defaults['jobName'] = '###JOBNAME###'
        js_defaults['jobDefinitionID'] = '###JOBDEFINITIONID###'
        js_defaults['destinationDBlock'] = '###OUTPUTDATASETNAME###'
        js_defaults['prodDBlock'] = '###PRODDBLOCK###'
        return js_defaults


    def get_jobspec_fields(self):
        """
        get_jobspec_fields ... mapping JobSpec() attributes (case sensitive) to fields from PanDA monitor (case insensitive).
        JobSpec() object is declared in /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/PandaClient/current/lib/python2.6/site-packages/pandatools/JobSpec.py
        """
        jobspec_fields = {}
        self.jobspec_attributes = ('PandaID','jobDefinitionID','schedulerID','pilotID','creationTime','creationHost',
                   'modificationTime','modificationHost','AtlasRelease','transformation','homepackage',
                   'prodSeriesLabel','prodSourceLabel','prodUserID','assignedPriority','currentPriority',
                   'attemptNr','maxAttempt','jobStatus','jobName','maxCpuCount','maxCpuUnit','maxDiskCount',
                   'maxDiskUnit','ipConnectivity','minRamCount','minRamUnit','startTime','endTime',
                   'cpuConsumptionTime','cpuConsumptionUnit','commandToPilot','transExitCode','pilotErrorCode',
                   'pilotErrorDiag','exeErrorCode','exeErrorDiag','supErrorCode','supErrorDiag',
                   'ddmErrorCode','ddmErrorDiag','brokerageErrorCode','brokerageErrorDiag',
                   'jobDispatcherErrorCode','jobDispatcherErrorDiag','taskBufferErrorCode',
                   'taskBufferErrorDiag','computingSite','computingElement','jobParameters',
                   'metadata','prodDBlock','dispatchDBlock','destinationDBlock','destinationSE',
                   'nEvents','grid','cloud','cpuConversion','sourceSite','destinationSite','transferType',
                   'taskID','cmtConfig','stateChangeTime','prodDBUpdateTime','lockedby','relocationFlag',
                   'jobExecutionID','VO','pilotTiming','workingGroup','processingType','prodUserName',
                   'nInputFiles','countryGroup','batchID','parentID','specialHandling','jobsetID',
                   'coreCount','nInputDataFiles','inputFileType','inputFileProject','inputFileBytes',
                   'nOutputDataFiles','outputFileBytes','jobMetrics')
        for attribute in self.jobspec_attributes:
            jobspec_fields[attribute] = attribute
            jobspec_fields[attribute.lower()] = attribute
        return jobspec_fields


    def get_filespec_fields(self):
        """
        get_filespec_fields ... mapping FileSpec() attributes (case sensitive) to fields from PanDA monitor (case insensitive).
        FileSpec() object is declared in /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/PandaClient/current/lib/python2.6/site-packages/pandatools/FileSpec.py
        """
        filespec_fields = {}
        #### do not set destinationDBlockToken ! HC-571
        self.filespec_attributes = ('rowID','PandaID','GUID','lfn','type','dataset','status','prodDBlock',
                   'prodDBlockToken','dispatchDBlock','dispatchDBlockToken','destinationDBlock',
                   'destinationSE','fsize','md5sum','checksum','scope')
        for attribute in self.filespec_attributes:
            filespec_fields[attribute] = attribute
            filespec_fields[attribute.lower()] = attribute
        return filespec_fields


    def load_json(self, filename):
        result=None
        try:
            f=open(filename, 'r')
            result=json.loads(f.read())
            f.close()
        except:
            traceback.print_exc()
        return result


    def read_jobparameters(self, filename):
        data = ''
        try:
            f=open(filename, 'r')
            data = f.read()
            f.close()
        except:
            traceback.print_exc()
        return data


    def write_file(self, data, filename):
        f=open(filename, 'w')
        f.write(data)
        f.close()


    def load_data(self):
        self.data = self.load_json(self.filename_input)
        if self.buildpandaid is not None:
            self.builddata = self.load_json(self.filename_input_build)


    def prepare_file_config(self, file_id, file_category_fields, file_content, jobtype='work', is_input=False):
        for field in file_category_fields:
            k = field.lower()
            if is_input and field=='status':
                v = 'ready'
            else:
                v = file_content[k]
            if jobtype == 'work':
                self.result['files']['%s.%s' % (file_id, field)] = v
            else:
                self.result['buildfiles']['%s.%s' % (file_id, field)] = v


    def process_files_of_category(self, file_list, category_fields, prefix, jobtype='work', is_input=False):
        idx = 0
        if jobtype == 'work':
            key_extra = 'files_extra'
        else:
            key_extra = 'buildfiles_extra'

        for f in file_list:
            file_id = '%s%02d' % (prefix,idx)
            file_lfn = 'LFN::'+f['lfn']
            self.prepare_file_config(file_id, category_fields, f, jobtype=jobtype, is_input=is_input)
            self.result[key_extra][file_lfn] = '###%s###' % (file_id)
            if 'list_files' not in self.result[key_extra].keys():
                self.result[key_extra]['list_files'] = '%s' % (file_id)
            else:
                self.result[key_extra]['list_files'] += ',%s' % (file_id)
            idx += 1


    def process_input_dspatterns(self, file_list, field_type='input', field='dataset'):
        patterns = []
        for f in file_list:
            if 'type' in f and f['type'] == field_type:
                ds = f[field]
                patterns.append(ds)
        patterns = sorted(list(set(patterns)))
        idx = 0
        for pattern in patterns:
            pattern_id = 'INPUTDSPATTERN%02d' % (idx)
            pattern_nfiles = len([x for x in file_list if x[field]==pattern])
            self.result['input_data_exploration_extra']['%s.dsn' % (pattern_id)] = pattern
            self.result['input_data_exploration_extra']['%s.nfiles' % (pattern_id)] = pattern_nfiles
            idx += 1


    def process_files(self, jobtype='work'):
        ### read info about files
        if jobtype == 'work':
            files = self.data['files']
        elif jobtype == 'build':
            files = self.builddata['files']
        ### categorize files
        inputs = [x for x in files if x['type'] == 'input']
        pseudo_inputs = [x for x in files if x['type'] == 'pseudo_input']
        outputs = [x for x in files if x['type'] == 'output']
        logs = [x for x in files if x['type'] == 'log']
        ### get prodDBlock
        prodDBlock = None
        if len(inputs):
            input0=inputs[0]
            try:
                field_name = self.filespec_fields['prodDBlock']
                prodDBlock = input0[field_name.lower()]
            except:
                traceback.print_exc()
            self.result['job_extra'] = {'prodDBlock': prodDBlock}
        if jobtype == 'build':
            try:
                output0 = outputs[0]
                field_name = self.filespec_fields['destinationDBlock']
                destinationDBlock = output0[field_name.lower()]
                prodDBlock = destinationDBlock
                self.result['buildjob_extra'] = {'prodDBlock': prodDBlock, 'destinationDBlock': destinationDBlock}
            except:
                traceback.print_exc()


        ### prepare config output
        ### prepare inputs
        self.process_files_of_category(inputs, self.filespec_fields_selected_in, 'fileI', jobtype=jobtype, is_input=True)
        if jobtype == 'work':
            self.process_input_dspatterns(inputs, field_type='input', field='dataset')
        ### prepare pseudo_inputs
        self.process_files_of_category(pseudo_inputs, self.filespec_fields_selected_in, 'filePS', jobtype=jobtype)
        ### prepare outputs
        self.process_files_of_category(outputs, self.filespec_fields_selected_out, 'fileO', jobtype=jobtype)
        ### prepare logs
        self.process_files_of_category(logs, self.filespec_fields_selected_out, 'fileOL', jobtype=jobtype)


    def process_job(self, jobtype='work'):
        ### read info about files
        if jobtype == 'work':
            job = self.data['job']
        elif jobtype == 'build':
            job = self.builddata['job']
        ### get selected job properties
        for field in self.jobspec_fields_selected:
            k = field.lower()
            v = job[k]
            if jobtype == 'work':
                self.result['job'][field] = v
            elif jobtype == 'build':
                self.result['buildjob'][field] = v


    def format_result(self):
        res = ''

        if self.buildpandaid is not None:
            buildjob_example = 'and buildjob %s' % (self.get_job_url(pandaid=self.buildpandaid))
        else:
            buildjob_example = ''

        ### header
        res += '''### clone of workjob %s %s
### HC template draft, created on %s
###
''' % (self.get_job_url(pandaid=self.pandaid), buildjob_example, self.hash)

        ### list of jobs: build vs. workjob
        res += '''
################
##### Jobs #####
################
[jobs] '''
        if self.buildpandaid is not None:
            res += '''
### TODO FIXME ###
### when you expect build job
list_jobs = buildjob,workjob
#
### when you do not expect build job
# list_jobs = workjob
#
'''
        else:
            res += '''
### TODO FIXME ###
### when you expect build job
# list_jobs = buildjob,workjob
#
### when you do not expect build job
list_jobs = workjob
#
'''

        ### input data exploration: exact vs. random
        res += '''
################################
#### Input data exploration ####
################################
[input_data_exploration]
### TODO FIXME ###
### hardcode dataset pattern and list of input files:
###     if you choose data_exploration_method = exact, make sure you
###     specify all the input dataset names or input container names as DSpatterns for the template.
###     They have to be the exact string as you have in this file below.
# data_exploration_method = exact
#
###
### provide pairs of {dataset pattern, number of files per dataset pattern}:
###     if you choose data_exploration_method = exact, make sure you
###     remove all the input file identifiers (starting with "fileI") from "list_files" below.
### the dataset pattern string should be the same as in HC template admin.
data_exploration_method = random
#
'''
        input_pattern_ids = []
        for item in self.result['input_data_exploration_extra'].keys():
            pattern_id = item.split('.')[0]
            input_pattern_ids.append(pattern_id)
        input_pattern_ids = sorted(list(set(input_pattern_ids)))
        res += '''

### In case you would like to use multiple input dataset name patterns to pick N input files,
### provide a space-delimited list of the input dataset name patterns.
###     e.g.
###    INPUTDSPATTERN00.dsn = mc12_8TeV:mc12_8TeV.*evgen.EVNT*   valid2:valid2.*evgen.EVNT*
###    INPUTDSPATTERN00.nfiles = 1
###

### TODO FIXME ###
'''
        res += 'list_input_dspattern_identifiers = %s' % (','.join(input_pattern_ids))
        for pattern_id in input_pattern_ids:
#             keys = sorted([x for x in input_pattern_ids if x.startswith('%s.'%(pattern_id))])
            key_dsn = '%s.dsn' % (pattern_id)
            value_dsn = self.result['input_data_exploration_extra'][key_dsn]
            key_nfiles = '%s.nfiles' % (pattern_id)
            value_nfiles = self.result['input_data_exploration_extra'][key_nfiles]
            res += '''
### input dspattern %(pattern_id)s
%(pattern_id)s.dsn = %(dsn)s
%(pattern_id)s.nfiles = %(nfiles)s
#
''' % {'pattern_id':pattern_id, 'dsn':value_dsn, 'nfiles':value_nfiles }

        if self.result['job']['transformation'].startswith('run'):
            transformation_prefix='http://pandaserver.cern.ch:25085/trf/user/'
        else:
            transformation_prefix=''

        ### workjob JobSpec
        try:
            my_prodDBlock = self.result['job_extra']['prodDBlock']
        except:
            my_prodDBlock = 'NULL'
        res += '''
###########################
##### workjob JobSpec #####
###########################
[workjob.jobspec]
### every property below that starts with " job. " is a JobSpec() object attribute.
###     Attribute names are case-sensitive.
###     List of JobSpec() attribute is in PanDA client on cvmfs, as well as in https://github.com/PanDAWMS/panda-client/blob/master/pandatools/JobSpec.py#L8
### every property below that does not start with " job. " is a variable that will be used in test definition.
###     You can refer to these variable values with their names in format ###VARIABLENAME### (variable name in all caps)
###     e.g.
###     prodDBlock will be known as ###PRODDBLOCK###
###


### TODO FIXME ###
### if there should be no build job, set     NOBUILD = True
NOBUILD = True
### if there should be build job, set     NOBUILD = False
# NOBUILD = False


## computingSite will be ###SITE###
job.computingSite = ###SITE###
## cloud will be ###CLOUD###
job.cloud = ###CLOUD###

## OUTPUTDATASETNAME will be ###OUTPUTDATASETNAME###

## prodDBlock will be ###PRODDBLOCK###
##        if you use "exact" input data exploration method, use dataset name of the first input file
prodDBlock = %(prodDBlock)s

## jobName will be ###JOBNAME### generated by HC
job.jobName = ###JOBNAME###

## jobDefinitionID will be ###JOBDEFINITIONID### generated by HC
job.jobDefinitionID = ###JOBDEFINITIONID###

job.AtlasRelease = %(AtlasRelease)s
job.homepackage = %(homepackage)s
job.transformation = %(transformation_prefix)s%(transformation)s
job.destinationDBlock = ###OUTPUTDATASETNAME###
job.destinationSE = ###SITE###
job.prodDBlock = ###PRODDBLOCK###
job.VO = %(VO)s
job.coreCount = ###CORECOUNT###
job.currentPriority = 10000
job.cmtConfig  = %(cmtConfig)s
job.prodUserName = gangarbt
job.inputFileType = %(inputFileType)s
### TODO FIXME ###
### you may wish to edit job.prodSourceLabel
job.prodSourceLabel = %(prodSourceLabel)s
### you may wish to edit job.processingType
job.processingType = gangarobot-celt

''' % \
{\
        'prodDBlock': my_prodDBlock,\
        'AtlasRelease': self.result['job']['AtlasRelease'],\
        'homepackage': self.result['job']['homepackage'],\
        'transformation': self.result['job']['transformation'],\
        'transformation_prefix': transformation_prefix, \
        'VO': self.result['job']['VO'],\
        'prodSourceLabel': self.result['job']['prodSourceLabel'],\
        'cmtConfig': self.result['job']['cmtConfig'],\
        'inputFileType': self.result['job']['inputFileType'],\
         }

         ### jobParameters
        res += '''

### jobParameters
## for data_exploration_method = random,
##     you can refer to 1st input file (index 0) with inputDS pattern file:
##     e.g.
##        FILEI00 = ###INPUTDSPATTERN00.file0###
##        FILEI01 = ###INPUTDSPATTERN00.file1###
##        FILEI02 = ###INPUTDSPATTERN01.file0###
##        FILEI03 = ###INPUTDSPATTERN02.file0###
#
### TODO FIXME ###'''
        for item in sorted(self.result['job_extra']):
            if item.startswith('###'):
                item_id = re.sub('###', '', item)
                res+= '''
%(key)s = %(value)s''' % {'key':item_id, 'value': self.result['job_extra'][item]}

        res += '''
### TODO FIXME ###
job.jobParameters = %(jobParameters)s
''' % {'jobParameters': self.result['job']['jobParameters']}

#         sys.stdout.flush()

        ### files header
        files_exact_list = sorted(list(set([x.split('.')[0] for x in self.result['files'].keys()])))
        files_exact = ','.join(files_exact_list)
        files_random = ','.join(sorted([x for x in files_exact_list if self.result['files']['%s.type'%(x)] != 'input']))
        res += '''
#########################
##### workjob Files #####
#########################
[workjob.files]
### every property below that starts with fileID listed in list_files is a FileSpec() object attribute.
###     Attribute names are case-sensitive.
###     List of FileSpec() attribute is in PanDA client on cvmfs, as well as in https://github.com/PanDAWMS/panda-client/blob/master/pandatools/FileSpec.py#L8
### You can refer to these variable values with their names in format ###variableName### (variable name is case sensitive).
###     HC then takes the fileID's lfn value to replace it e.g. in jobParameters
###     e.g.
###     attribute fileI00.lfn:
###            fileI00.lfn = my.file.name.root
###     will be used to define variable MYINPUTFILE:
###            MYINPUTFILE = ###fileI00###
###     and job.jobParameters will then include  " ###MYINPUTFILE### "  e.g.
###             --inputEvgenFile ###MYINPUTFILE###

##########################
##### workjob Inputs #####
##########################
## first, list names of file objects
### TODO FIXME ###
## for data_exploration_method = exact, list inputs, pseudoinputs, outputs, and logs
# list_files = %(files_exact)s
## for data_exploration_method = random, do not list inputs, list only pseudoinputs, outputs, and logs
list_files = %(files_random)s
#
## provide configuration for all input file objects
## file object configuration format:
##     fileID.field = value
## fields for inputs/pseudo_inputs: type, lfn, dataset, prodDBlock, GUID, fsize, scope, status='ready', md5sum, checksum
## fields for logs/outputs: type, lfn, dataset, destinationDBlock, destinationSE
####### N.B.: Do not set destinationDBlockToken ! HC-571
##

''' %  {'files_exact':files_exact, 'files_random':files_random}

        ### files: inputs
        res += '''
##########################
##### workjob Inputs #####
##########################
## for data_exploration_method = random, the workjob Inputs are ignored.
### TODO FIXME ###
###
####### N.B.: BigPanDA monitor shows file sizes in MB, while PanDA expects size in Bytes.
#######       If you are using data_exploration_method = exact , you may need to provide exact size of file in Bytes in fsize field.

'''
        ### loop over inputs files
        res += self.format_files(files_exact_list, 'input', jobtype='work')
        ### loop over pseudo_inputs files
        res += self.format_files(files_exact_list, 'pseudo_input', jobtype='work')

        ### files: outputs
        res += '''

###########################
##### workjob Outputs #####
###########################
### consider setting output's and log's "dataset" and "destinationDBlock" to ###OUTPUTDATASETNAME###
### consider setting lfn to value dependent on ###JOBNAME##, or on ###TESTID###
###     e.g.
###
###     ### file fileOL00
###     fileOL00.dataset = ###OUTPUTDATASETNAME###
###     fileOL00.destinationDBlock = ###OUTPUTDATASETNAME###
###     fileOL00.lfn = ###JOBNAME###.job.log.tgz
###     fileOL00.type = log
###     fileOL00.destinationSE = ###SITE###
###
###     ### file fileO00
###     fileO00.dataset = ###OUTPUTDATASETNAME###
###     fileO00.destinationDBlock = ###OUTPUTDATASETNAME###
###     fileO00.lfn = Hits.hc_###TESTID###.###SITE###.###JOBNAME###.HITS.pool.root
###     fileO00.type = output
###     fileO00.destinationSE = ###SITE###
###
#
### TODO FIXME ###
#'''
        ### loop over logs files
        res += self.format_files(files_exact_list, 'log', jobtype='work')
        ### loop over outputs files
        res += self.format_files(files_exact_list, 'output', jobtype='work')



        if self.buildpandaid is not None:

            if self.result['buildjob']['transformation'].startswith('build'):
                buildtransformation_prefix='http://pandaserver.cern.ch:25085/trf/user/'
            else:
                buildtransformation_prefix=''

            ### buildjob JobSpec
            res += '''
############################
##### buildjob JobSpec #####
############################
[buildjob.jobspec]
## computingSite will be ###SITE###
job.computingSite = ###SITE###
## cloud will be ###CLOUD###
job.cloud = ###CLOUD###

## OUTPUTDATASETNAME will be ###OUTPUTDATASETNAME###

## prodDBlock will be ###PRODDBLOCK###
##        if you use "exact" input data exploration method, use dataset name of the first input file
prodDBlock = %(prodDBlock)s

## jobName will be ###JOBNAME### generated by HC
job.jobName = ###JOBNAME###

## jobDefinitionID will be ###JOBDEFINITIONID### generated by HC
job.jobDefinitionID = ###JOBDEFINITIONID###

job.AtlasRelease = %(AtlasRelease)s
job.homepackage = %(homepackage)s
job.transformation = http://pandaserver.cern.ch:25085/trf/user/%(transformation)s
job.destinationDBlock = ###OUTPUTDATASETNAME###
job.destinationSE = ###SITE###
job.VO = %(VO)s
job.coreCount = ###CORECOUNT###
job.prodSourceLabel = %(prodSourceLabel)s
job.processingType = gangarobot-celt
job.currentPriority = 10000
job.cmtConfig  = %(cmtConfig)s
job.prodUserName = gangarbt
job.nOutputDataFiles = 1
job.destinationSE = ###SITE###



''' % \
{\
        'prodDBlock': self.result['buildjob_extra']['prodDBlock'],\
        'AtlasRelease': self.result['buildjob']['AtlasRelease'],\
        'homepackage': self.result['buildjob']['homepackage'],\
        'transformation': self.result['buildjob']['transformation'],\
        'transformation_prefix': buildtransformation_prefix, \
        'VO': self.result['buildjob']['VO'],\
        'prodSourceLabel': self.result['buildjob']['prodSourceLabel'],\
        'cmtConfig': self.result['buildjob']['cmtConfig'],\
         }

             ### jobParameters
            res += '''

### jobParameters
'''
            for item in sorted(self.result['buildjob_extra']):
                if item.startswith('###'):
                    item_id = re.sub('###', '', item)
                    res+= '''
%(key)s = %(value)s''' % {'key':item_id, 'value': self.result['buildjob_extra'][item]}

            res += '''
#job.jobParameters = %(jobParameters)s
job.jobParameters =
''' % {'jobParameters': self.result['buildjob']['jobParameters']}

            ### files header
            files_exact_list = sorted(list(set([x.split('.')[0] for x in self.result['buildfiles'].keys()])))
            files_exact = ','.join(files_exact_list)
            files_random = ','.join(sorted([x for x in files_exact_list if self.result['buildfiles']['%s.type'%(x)] != 'input']))
            res += '''
##########################
##### buildjob Files #####
##########################
[buildjob.files]

###########################
##### buildjob Inputs #####
###########################
## first, list names of file objects
# list_files = %(files_exact)s
list_files = %(files_random)s

## provide configuration for all input file objects
## file object configuration format:
##     fileID.field = value
## fields for inputs/pseudo_inputs: type, lfn, dataset, prodDBlock
## fields for logs/outputs: type, lfn, dataset, destinationDBlock, destinationSE
##

''' %  {'files_exact':files_exact, 'files_random':files_random}

            ### files: inputs
            res += '''
###########################
##### buildjob Inputs #####
###########################
## for data_exploration_method = random the buildjob Inputs are ignored.
'''
            ### loop over inputs files
            res += self.format_files(files_exact_list, 'input', jobtype='build')
            ### loop over pseudo_inputs files
            res += self.format_files(files_exact_list, 'pseudo_input', jobtype='build')

            ### files: outputs
            res += '''

############################
##### buildjob Outputs #####
############################
## consider setting output's and log's "dataset" and "destinationDBlock" to ###OUTPUTDATASETNAME###

'''
            ### loop over logs files
            res += self.format_files(files_exact_list, 'log', jobtype='build')
            ### loop over outputs files
            res += self.format_files(files_exact_list, 'output', jobtype='build')

        return res


    def format_files(self, all_file_ids, file_type='input', jobtype='work'):
        res = ''
        if jobtype == 'work':
            files_key = 'files'
        elif jobtype == 'build':
            files_key = 'buildfiles'
        ### loop over all files of the type
        files_ids = [x for x in all_file_ids if self.result[files_key]['%s.type'%(x)] == file_type]
        for fid in files_ids:
            file_properties = sorted([x for x in self.result[files_key] if x.startswith('%s.' % (fid))])
            res += '''
### file %(fid)s'''  % {'fid':fid}
            for fp in file_properties:
                k = fp
                v = self.result[files_key][k]
                res += '''
%(k)s = %(v)s''' % {'k':k, 'v':v}
            if file_type in ['log', 'output']:
                res += '''
%(fid)s.destinationSE = ###SITE###
'''  % {'fid':fid}
            res += '''

'''

        return res



if __name__ == '__main__':
    try:
        argv = sys.argv[1:]
        if len(argv)==2:
            print('INFO\tRunning:\t%s %s' %(sys.argv[0], ' '.join(argv) ))
            pandaid = argv[0]
            jobparametersfile = argv[1]
            buildpandaid = None
            buildjobparametersfile = ''
        elif len(argv)==4:
            print('INFO\tRunning:\t%s %s' %(sys.argv[0], ' '.join(argv) ))
            pandaid = argv[0]
            jobparametersfile = argv[1]
            buildpandaid = argv[2]
            buildjobparametersfile = argv[3]

        if len(argv)<2:
            print('ERROR\tUsage:\t%s PandaID /path/to/file/with/jobparameters.txt buildPandaID /path/to/file/with/buildjobparameters.txt' % (sys.argv[0]))
            exit(1)
        else:
            print('INFO\tUsage:\t%s PandaID /path/to/file/with/jobparameters.txt buildPandaID /path/to/file/with/buildjobparameters.txt' % (sys.argv[0]))

        cj = ConfigureJob(pandaid=pandaid, jobparametersfile=jobparametersfile, buildpandaid=buildpandaid, buildjobparametersfile=buildjobparametersfile)
        cj.run()

    except Exception:
        traceback.print_exc()
    else:
        print('INFO\tAll succeded - Over and out')


