configurejob.py
===


The `configurejob.py` script prepares draft of HammerCloud template configuration file for PanDA job. 

You need to provide PanDA job ID, and path to a file with job's jobParameters (copy&paste it from the BigPanDA monitor job page). 

Howto
===
1. Pick a `PanDA job ID` of a job that you desire to run in HammerCloud.
2. Copy jobParameters of your desired job to a file, e.g. `/tmp/example-jobparameters.txt`
3. Run the script: 

```
PANDAJOBID=2981020666
JOBPARAMETERS_FILE=/tmp/example-jobparameters.txt
python configurejob.py ${PANDAJOBID} ${JOBPARAMETERS_FILE}
```

Draft of the template file will be stored in a file. The script notifies you about the filename: 

```
INFO    Wrote result in file /tmp/draft_template_from_2981020666.tpl.2016-09-06.175444
```

Edit the template file if needed, and upload the file to the desired location to make it available for test submission.  



Examples
===


Example of a PanDA job page: [http://bigpanda.cern.ch/job?pandaid=2981020666](http://bigpanda.cern.ch/job?pandaid=2981020666). 

Example of jobparameters: 

```
--inputBSFile=data15_13TeV.00284285.physics_Main.daq.RAW._lb0223._SFO-1._0001.data --outputAODFile=AOD.09329776._000001.pool.root.1 --outputDAOD_IDTIDEFile=DAOD_IDTIDE.09329776._000001.pool.root.1 --outputDESDM_CALJETFile=DESDM_CALJET.09329776._000001.pool.root.1 --outputDESDM_EGAMMAFile=DESDM_EGAMMA.09329776._000001.pool.root.1 --outputDESDM_EXOTHIPFile=DESDM_EXOTHIP.09329776._000001.pool.root.1 --outputDESDM_MCPFile=DESDM_MCP.09329776._000001.pool.root.1 --outputDESDM_PHOJETFile=DESDM_PHOJET.09329776._000001.pool.root.1 --outputDESDM_SGLELFile=DESDM_SGLEL.09329776._000001.pool.root.1 --outputDESDM_SLTTMUFile=DESDM_SLTTMU.09329776._000001.pool.root.1 --outputDRAW_EGZFile=DRAW_EGZ.09329776._000001.pool.root.1 --outputDRAW_EMUFile=DRAW_EMU.09329776._000001.pool.root.1 --outputDRAW_RPVLLFile=DRAW_RPVLL.09329776._000001.pool.root.1 --outputDRAW_TAUMUHFile=DRAW_TAUMUH.09329776._000001.pool.root.1 --outputDRAW_ZMUMUFile=DRAW_ZMUMU.09329776._000001.pool.root.1 --outputHISTFile=HIST.09329776._000001.pool.root.1 --jobNumber=1 --maxEvents="default:-1" --postExec "ESDtoDPD:from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCMapfromCool=False;ServiceMgr.MuonRPC_CablingSvc.CorrFileName=\"LVL1confAtlasRUN2_ver016.corr\";ServiceMgr.MuonRPC_CablingSvc.ConfFileName=\"LVL1confAtlasRUN2_ver016.data\";" "RAWtoESD:ToolSvc.MuidRefitTool.DeweightEEL1C05=True;ToolSvc.OutwardsRefitTool.DeweightEEL1C05=True;" --preExec "all:DQMonFlags.enableLumiAccess=False;" "RAWtoESD:from LArConditionsCommon.LArCondFlags import larCondFlags;larCondFlags.OFCShapeFolder.set_Value_and_Lock(\"4samples1phase\");" --autoConfiguration=everything --beamType=collisions --conditionsTag "default:CONDBR2-BLKPA-2016-16" --geometryVersion="default:ATLAS-R2-2015-04-00-00" --runNumber=284285 --AMITag=r8392 --ignoreErrors=True --ignorePatterns='ToolSvc.InDetSCTRodDecoder.+ERROR.+Unknown.+offlineId.+for.+OnlineId'
```

