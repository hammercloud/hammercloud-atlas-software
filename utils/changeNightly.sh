#!/bin/bash

# changeNightly.sh ... updates nighly version in template 944 
#
# Copyright European Organization for Nuclear Research (CERN) 
# This software is distributed under the terms of the 
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the 
# file COPYING. 
# In applying this licence, CERN does not waive the privileges and immunities 
# granted to it by virtue of its status as an Intergovernmental Organization or 
# submit itself to any jurisdiction. 
# 
# 
# Authors: 
# - Federica Legger, <jaroslava.schovancova@cern.ch>, 2017 
# 
# 

#runs on hc11 as hcuser
cd /data/hc/apps/atlas/inputfiles/templates/nightly/

#get nightlies
#ls not very robust
#NEW=$(ls  --color=never -Art /cvmfs/atlas-nightlies.cern.ch/repo/sw/21.0 | tail -n 1)

source /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh Athena,21.0,latest,opt
which athena
NEW=$(which athena |grep -o "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9][0-9][0-9]")

#if NEW nightly is found, then replace
if [[ $NEW =~ ^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T[0-9]{4}$ ]]
then
    OLD=$(grep -o "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9][0-9][0-9]" HelloWorld_nightly.tpl)
    echo "Replacing "$OLD" with "$NEW
    echo "Issued command sed -i 's/$OLD/$NEW/g' HelloWorld_nightly.tpl"
    sed -i "s/$OLD/$NEW/g" HelloWorld_nightly.tpl
    grep -i AnalysisTransforms-Athena HelloWorld_nightly.tpl
 
else
    echo "ERROR: WRONG FORMAT, cannot replace "$OLD" with "$NEW
fi
