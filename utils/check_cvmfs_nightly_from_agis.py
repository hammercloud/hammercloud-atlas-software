"""
# check_cvmfs_nightly_from_agis.py ... Check cvmfs nightly presence in AGIS
#
#
#
# Copyright European Organization for Nuclear Research (CERN)
#
# This software is distributed under the terms of the
# GNU General Public Licence version 3 (GPL Version 3), copied verbatim in the
# file COPYING.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.
#
#
# Authors:
# - Jaroslava Schovancova, <jaroslava.schovancova@cern.ch>, 2016-
#
#
"""


import json as simplejson
import urllib2



class Checker:


    def __init__(self):
        self.agis_schedconf_json_url = "http://atlas-agis-api.cern.ch/request/pandaqueue/query/list/?json&preset=schedconf.all"
        self.NIGHTLIES_REPO = '/cvmfs/atlas-nightlies.cern.ch/repo/sw/nightlies'


    def get_agis_json(self):
        self.pandaqueues_all = simplejson.load(urllib2.urlopen(self.agis_schedconf_json_url))
        self.pq = {}
        for k in self.pandaqueues_all:
            v = self.pandaqueues_all[k]
            if v['type'] == 'analysis' and self.NIGHTLIES_REPO in v['appdir'] and v['state']=='ACTIVE' and 'AFT' in str(v['hc_suite']):
                self.pq[k] = v


    def print_results(self):
        print('List of PQs from http://atlas-agis-api.cern.ch/request/pandaqueue/query/list/?json&preset=schedconf.all')
        print('that are state=ACTIVE , type=analysis , have %s in appdir, and run AFT:' % (self.NIGHTLIES_REPO))
        print('')
        print('')
        print('')
        print(', '.join(sorted(self.pq)))


    def run(self):
        self.get_agis_json()
        self.print_results()


if __name__ == '__main__':
    Checker().run()

